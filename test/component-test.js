import React from 'react/addons';
import BlogFeatures from '../src/component.jsx';
import mockedData from './mockData.js';

// Assign TestUtils library
const TestUtils = React.addons.TestUtils;

describe('BlogFeatures Component: ', () => {
  const renderer = TestUtils.createRenderer();
  let shallowComponent;

  it('should be defined', () => {
    const component = TestUtils.renderIntoDocument(<BlogFeatures />);
    expect(component).toBeTruthy();
  });

  it('should be a React component', () => {
    const isElement = TestUtils.isElement(<BlogFeatures />);
    expect(isElement).toBeTruthy();
  });

  // Error States depend on prop.items
  describe('Error States: ', () => {
    describe('No prop.items are passed', () => {
      beforeEach(() => {
        renderer.render(<BlogFeatures />);
        shallowComponent = renderer.getRenderOutput();
      });

      it('should return an error message', () => {
        expect(shallowComponent.props.children).toEqual('No data available...');
      });

      it('should return an error class with a DIV element', () => {
        expect(shallowComponent.props.className).toEqual('errorMessage');
        expect(shallowComponent.type).toEqual('div');
      });
    });

    describe('An empty array is passed to props.items', () => {
      beforeEach(() => {
        renderer.render(<BlogFeatures items={[]} />);
        shallowComponent = renderer.getRenderOutput();
      });

      it('should return an error message', () => {
        expect(shallowComponent.props.children).toEqual('No data available...');
      });

      it('should return an error class with a DIV element', () => {
        expect(shallowComponent.props.className).toEqual('errorMessage');
        expect(shallowComponent.type).toEqual('div');
      });
    });
  });

  // Success State
  describe('Success State with all expected data: ', () => {
    beforeEach(() => {
      renderer.render(<BlogFeatures items={mockedData.full} />);
      shallowComponent = renderer.getRenderOutput();
    });

    it('should return the correct class with DIV element', () => {
      expect(shallowComponent.props.className).toEqual('blogFeatures');
      expect(shallowComponent.type).toEqual('div');
      expect(shallowComponent.props.id).toEqual('blogFeatures');
    });
  });
});
