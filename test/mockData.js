const full = [
  {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption01',
      },
    },
    image: {
      rectangularImage: 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
    },
  }, {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption02',
      },
    },
    image: {
      rectangularImage: 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
    },
  },
  {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption03',
      },
    },
    image: {
      rectangularImage: 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
    },
  },
];

export default {
  full,
};
