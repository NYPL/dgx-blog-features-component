## CHANGELOG

### v0.3.1
- Updating SVG icon prop.

### v0.3.0
- Updating the SVG icon set to use the @nypl package.

### v0.2.0
- Updating to React 15.
