import React from 'react';
import PropTypes from 'prop-types';
import { LionLogoIcon } from '@nypl/dgx-svg-icons';

const BlogFeatures = ({
  className,
  id,
  lang,
  items,
  itemsToDisplay,
  errorMessage,
  gaClickEvent,
}) => {
  /**
   * isDataInvalid(data)
   * returns boolean check if the data array exists or not.
   */
  const isDataInvalid = data => (!data || !data.length);

  /**
   * renderContentTitle(bemName, title, language) {
   * returns the DOM representation of the main content title
   * composed of the description and language properties.
   * Each property is namespaced via bemName and verified for existance.
   */
  const renderContentTitle = (bemName, title, language) => (
    (title && title[language] && title[language].text) ?
      <p className={`${bemName}-title`}>{title[language].text}</p> : null
  );

  /**
   * renderContentImage(image)
   * returns the DOM representation of an image only if the data exists.
   */
  const renderContentImage = (image) => {
    const imageUri = (image && image.rectangularImage && image.rectangularImage['full-uri']) ?
      image.rectangularImage['full-uri'] : null;
    const alt = (image && image.rectangularImage && image.rectangularImage.alt) ?
      image.rectangularImage.alt : '';

    return (imageUri) ? <img src={imageUri} alt={alt} className="rectangularImage" /> : null;
  };

  /**
   * renderContentDesc(bemName, description, language) {
   * returns the DOM representation of the main content description
   * composed of the description and language properties.
   * Each property is namespaced via bemName and verified for existance.
   */
  const renderContentDesc = (bemName, description, language) => (
    (description && description[language] && description[language].text) ?
      <p className={`${bemName}-description`}>{description[language].text}</p> : null
  );

  /**
   * renderAuthorsBox(bemName, data)
   * returns the DOM representation of the author's section
   * composed of the image, name and title properties.
   * Each property is namespaced via bemName and verified for existance.
   */
  const renderAuthorsBox = (bemName, data) => {
    /* Returning null on empty data to follow
     * Facebook's component spec -- https://facebook.github.io/react/docs/component-specs.html
     */
    if (!data) {
      return null;
    }

    const classes = `${bemName}-authorBox`;
    // Destructure Object properties with default values
    const {
      title: authorTitle = null,
      firstName,
      lastName,
      image,
    } = data;

    const authorImage = (image && image['full-uri']) ?
      <img src={image['full-uri']} alt={image.description || ''} className={`${classes}-image`} />
      : <LionLogoIcon ariaHidden height={25} width={25} />;
    const title = (authorTitle) ? <p className={`${classes}-title`}>{authorTitle}</p> : null;
    const fullName = (firstName && lastName) ?
      <p className={`${classes}-name`}>{`${firstName} ${lastName}`}</p> : null;

    return (
      <div className={classes}>
        {authorImage}
        {fullName}
        {title}
      </div>
    );
  };

  /**
   * generateItemsToDisplay(items, itemsToDisplay, bemName, lang)
   * returns the DOM for each item to be displayed by iterating
   * through the data. Elements to be displayed are constructed
   * from helper render methods. Namespacing the component via
   * the BEM name and sets a limit of iteration through itemsToDisplay number.
   */
  const generateItemsToDisplay = data => (
    data.slice(0, itemsToDisplay).map((element, i) => {
      const {
        title,
        link: url = '#',
        image,
        description: desc,
        author,
      } = element;
      const authorBox = renderAuthorsBox(className, author);
      const mainImage = renderContentImage(image);
      const contentTitle = renderContentTitle(className, title, lang);
      const contentDesc = renderContentDesc(className, desc, lang);
      const gaAction = `From Our Blog - ${i + 1}`;

      return (
        <div className={`${className}-item item-${i}`} key={i}>
          <a
            className={`${className}-link`}
            href={url}
            onClick={gaClickEvent ? () => gaClickEvent(gaAction, url) : false}
          >
            <div className={`${className}-imageBox`}>
              {mainImage}
            </div>
            <div className={`${className}-contentBox`}>
              {contentTitle}
              {contentDesc}
              {authorBox}
            </div>
          </a>
        </div>
      );
    })
  );

  /**
   * renderFailure(msg)
   * returns the error DOM with appropriate error message
   */
  const renderFailure = msg => (
    <div className="errorMessage">
      {msg}
    </div>
  );

  /**
   * renderSuccess(data)
   * calls helper method generateItemsToDisplay which will iterate and return the correct DOM.
   */
  const renderSuccess = data => (
    <div className={`${className}`} id={id}>
      {generateItemsToDisplay(data)}
    </div>
  );

  return isDataInvalid(items) ? renderFailure(errorMessage) : renderSuccess(items);
};

BlogFeatures.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  lang: PropTypes.string,
  items: PropTypes.array,
  itemsToDisplay: PropTypes.number,
  errorMessage: PropTypes.string,
  gaClickEvent: PropTypes.func,
};

BlogFeatures.defaultProps = {
  className: 'blogFeatures',
  lang: 'en',
  itemsToDisplay: 2,
  errorMessage: 'We\'re sorry. Information isn\'t available for this feature.',
};

export default BlogFeatures;
