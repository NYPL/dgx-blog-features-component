import React from 'react';
import { render } from 'react-dom';
import BlogFeatures from './component';

const dummyContent = [
  {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption01',
      },
    },
    image: {
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'this is the image description',
        alt: 'this is the image alt',
      },
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
      image: {
        'full-uri': 'https://petrol.nypl.org/sites/default/files/PublicDomainTheather-TheBlackCrook.jpg',
        description: 'author image desc',
      },
    },
  }, {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption02',
      },
    },
    image: {
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'this is the image description',
        alt: 'this is the image alt',
      },
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
      image: null,
    },
  }, {
    title: {
      en: {
        type: 'text-single',
        text: "What's Your Literary Waterloo?",
      },
    },
    description: {
      en: {
        text: 'descrption03',
      },
    },
    image: {
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'this is the image description',
        alt: 'this is the image alt',
      },
    },
    link: 'nypl.org',
    author: {
      firstName: 'Gwen',
      lastName: 'Glazer',
      title: 'Recommendations Editor, Readers Services',
      image: {
        'full-uri': 'https://petrol.nypl.org/sites/default/files/' +
          'PublicDomainTheather-TheBlackCrook.jpg',
      },
    },
  },
];

// Used to mock gaClick event
// The action is passed while the label is returned in a new function
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

/* app.jsx
 * Used for local development of React Components
 */
render(
  <BlogFeatures
    className="hpBlogs"
    id="hpBlogs"
    items={dummyContent}
    itemsToDisplay={3}
    gaClickEvent={gaClickTest()}
  />, document.getElementById('component')
);
